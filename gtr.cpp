﻿#include <iostream>
#include <list>
#include <algorithm>

static std::list<int> list(10);

int generator() {
	static int i;
	return i += 3;
}
//Создать список
void generate_list() {
	std::generate(list.begin(), list.end(), generator);
}

int transformer(int value) {
	return value % 4 ? value % 4 : value;
}
//Изменить список
void transform_list() {
	std::transform(list.begin(), list.end(), list.begin(), transformer);
}

bool filter(int value) { return value > 3; }
//Фильтр списка
void remove_list() {
	auto remove = std::remove_if(list.begin(), list.end(), filter);
	list.erase(remove, list.end());
}

void cout() {
	std::copy(list.begin(), list.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;
}

int main()
{
	generate_list();
	cout();
	transform_list();
	cout();
	remove_list();
	cout();
}